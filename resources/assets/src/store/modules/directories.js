import axios from 'axios';

const state = {
    appDir: [],
    accDir: [],
    loaded: false,
};

const getters = {
    getAppDirectories: state => {
        return state.appDir;
    },
    getAccountDirectories: state => {
        return state.accDir;
    },
    getDirLoadState: state => {
        return state.loaded;
    },
};

const mutations = {
    setAppDirectory: (state, payload) => {
        state.appDir.push(payload);
    },
    setAccountDirectory: (state, payload) => {
        state.accDir.push(payload);
    },
    setLoadState: (state, payload) => {
        state.loaded = payload;
    },
    unsetAppDirectory: (state, payload) => {
        state.appDir.splice(payload, 1);
    },
    unsetAccountDirectory: (state, payload) => {
        state.accDir.splice(payload, 1);
    },
};


const actions = {
    async setDirectoriesAsync ( { commit } , payload ) {
        commit('setLoadState', 'wait' );
        let {data} = await axios.get('catalogs');
        let appDirectories = data.data.global;
        let accDirectories = data.data.local;
        for(let i = 0; i < appDirectories.length; i++){
            commit('setAppDirectory', appDirectories[i] );
        }
        for(let i = 0; i < accDirectories.length; i++){
            commit('setAccountDirectory', accDirectories[i] );
        }
        commit('setLoadState', true );
    },
    reloadDirectoriesUponRequest: ( { commit }, payload) => {
        commit('setLoadState', false );
    },
    async removeAccDirectoryAsync ( { commit }, payload)  {
        try {
            let {data} = await axios.delete('catalogs/' + payload.id);
            for (let i = 0; i < state.accDir.length; i++){
                if (state.accDir[i].id === payload.id){
                    commit('unsetAccountDirectory', i);
                }
            }
        } catch (err) {
            console.error('Failure!');
            console.error(err.response.status);
        }
    },
    async removeAppDirectoryAsync ( { commit }, payload)  {
        try {
            let {data} = await axios.delete('catalogs/' + payload.id);
            for (let i = 0; i < state.appDir.length; i++){
                if (state.appDir[i].id === payload.id){
                    commit('unsetAppDirectory', i);
                }
            }
        } catch (err) {
            console.error('Failure!');
            console.error(err.response.status);
        }
    },
};

export default {
    state,
    mutations,
    actions,
    getters
}