import axios from 'axios';

const state = {
    categoriesTree:[],
    categoriesList:[],
    groupsTree:[],
    groupsList:[],
    loadStatus: {
        groups: false,
        categories: false,
    }
};

const getters = {
    getGroups: state => {
        return state.groupsList;
    },
    getGroupsTree: state => {
        return state.groupsTree;
    },
    getGroupsStatus: state => {
        return state.loadStatus.groups;
    },
    getCategoriesList: state => {
        return state.categoriesList;
    },
    getCategoriesTree: state => {
        return state.categoriesTree;
    },
    getCategoriesStatus: state => {
        return state.loadStatus.categories;
    },
};

const mutations = {
    setCategoryData: (state, payload) => {
        recursiveSearch(payload,'categoriesList');
    },
    setCategoriesTree: (state, payload) => {
        state.categoriesTree = payload;
    },
    setCategoriesState: (state, payload) => {
        state.loadStatus.categories = payload;
    },
    setGroupData: (state, payload) => {
        state.groupsList = [];
        recursiveSearch(payload,'groupsList');
    },
    setGroupsTree: (state, payload) => {
        state.groupsTree = payload;
    },
    setGroupsState: (state, payload) => {
        state.loadStatus.groups = payload;
    },
};


const actions = {
    async setCategoriesAsync ( { commit } , payload ) {
        let {data} = await axios.get('category');
        commit('setCategoriesTree', data.data );
        commit('setCategoryData', data.data );
        commit('setCategoriesState', true );
    },
    setCategory: ( { commit }, payload) => {
        commit('setCategoryData', [payload]);
    },
    async setGroupsAsync ( { commit } , payload ) {
        commit('setGroupsState', false );
        let {data} = await axios.get('template-group');
        commit('setGroupsTree', data.data );
        commit('setGroupData', data.data );
        commit('setGroupsState', true );
    },
    setGroup: ( { commit }, payload) => {
        commit('setGroupData', [payload]);
    },
};

function recursiveSearch(data, list) {
    data.forEach(item =>{
        let newObj = {};
        let children = [];
        for (let key in item) {
            if (key === 'children'){
                children = item[key];
            } else {
                newObj[key] = item[key];
            }
        }
        state[list].push(newObj);
        if (children.length > 0){
            recursiveSearch(children, list);
        }
    });
}

export default {
    state,
    mutations,
    actions,
    getters
}