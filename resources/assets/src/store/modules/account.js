import axios from 'axios';

const state = {
    account: {
        'id' : false,
        'name' : '',
        'users' : [],
        'user_limit' : 5,
        'template_limit': 0,
    },
    accountTemplates:[],
    load: false,
};

const getters = {
    getAccount: state => {
        return state.account;
    },
    checkAccountLoad: state => {
        return state.load;
    },
    getAccountTemplates: state => {
        return state.accountTemplates;
    },
};

const mutations = {
    setAccount: (state, payload) => {
        for (let key in payload){
            if (key === 'templates') {
                state.accountTemplates = payload[key];
            } else {
                state.account[key] = payload[key];
            }
        }
    },
    setAccountLoad: (state, payload) => {
        state.load = payload;
    },
    updateAccount: (state, payload) => {
        for (let key in payload){
            state.account[key] = payload[key];
        }
    },
    setAccountUser: (state, payload) => {
        state.account.users.push(payload);
    },
    deleteAccountUser: (state, payload) => {
        state.account.users.splice(payload,1);
    },
    updateAccountUser: (state, payload) => {
        let data = payload.data;
        for (let key in data){
            state.account.users[payload.index][key] = data[key];
        }
    },
    setAccountTemplate: (state, payload) => {
        state.accountTemplates.push(payload);
    },
    delAccountTemplate: (state, payload) => {
        state.accountTemplates.splice(state.accountTemplates.findIndex(t => t.id === payload),1);
    },
};


const actions = {
    async setAccountAsync ( { commit } , payload ) {
        commit('setAccountLoad', false );
        let {data} = await axios.get('/accounts/' + payload.id);
        commit('setAccount', data.data );
        commit('setAccountLoad', true );

    },
    updateAccountData ( { commit } , payload) {
            commit('updateAccount', payload);
    },
    setAccountTemplateData ( { commit } , payload ) {
        commit( 'setAccountTemplate', payload );
    },
    delAccountTemplateData ( { commit } , payload ) {
        commit( 'delAccountTemplate', payload );
    },
    setAccountUserData ( { commit } , payload) {
        if(state.account.users.length < state.account.user_limit){
            console.log(true);
            commit( 'setAccountUser' , payload);
        }
    },
    deleteAccountUserData ( { commit } , payload) {
        commit( 'deleteAccountUser' , payload);
    },
    updateAccountUser ( { commit } , payload) {
        commit( 'updateAccountUser', payload);
    },
};

export default {
    state,
    mutations,
    actions,
    getters
}