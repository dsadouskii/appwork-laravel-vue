import Vue from 'vue';
import Vuex from 'vuex';

import account from './modules/account';
import directories from './modules/directories';
import navItems from './modules/navItems';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        account,
        directories,
        navItems,
    }
});