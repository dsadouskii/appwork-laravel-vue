import Test                 from '../components/Test.vue'
// import Index                from './components/inner-page/Index';
import FrontPage            from '../components/FrontPage.vue';
import Register             from '../components/auth/Register.vue';
import Login                from '../components/auth/Login.vue';

import Catalogs             from '../components/catalogs/Index.vue';
import CatalogsList         from '../components/catalogs/CatalogsList.vue';
import CatalogUpload        from '../components/catalogs/Upload.vue';
import CatalogShow          from '../components/catalogs/Show.vue';

import Profiles             from '../components/accounts/Index.vue';
import ProfilesList         from '../components/accounts/AccountsList.vue';
import ProfileShow          from '../components/accounts/Show.vue';
import TemplateSetup        from '../components/accounts/TemplateSetup.vue';
import AccountInfo          from '../components/accounts/AccountInfo.vue';
import AccountTemplates     from '../components/accounts/AccountTemplates.vue';
import AccTemplateCreate    from '../components/accounts/TemplateCreate.vue';
import AccountUsers         from '../components/accounts/AccountUsers.vue';

import TemplateIndex        from '../components/templates/Index.vue';
import TemplateList         from '../components/templates/TemplatesList.vue';
import TemplateEdit         from '../components/templates/Edit.vue';
import TemplateCreate       from '../components/templates/Create.vue';

import GroupsList           from '../components/templates/GroupsList.vue';
import GroupTemplates       from '../components/templates/GroupTemplates.vue';

import DocumentCreate       from '../components/documents/Create.vue';
import DocumentEdit         from '../components/documents/Edit.vue';
import DocumentShow         from '../components/documents/Show.vue';

import DocumentIndex        from '../components/documents/Index.vue';
import TemplatesList        from '../components/documents/TemplatesList.vue';
import UserTemplatesList    from '../components/documents/UserTemplatesList.vue';
import DocumentsList        from '../components/documents/DocumentsList.vue';
import DocumentsArchive     from '../components/documents/Archive.vue';

const AppRoutes = [
    {
        path: '/test',
        name: 'test',
        component: Test,
    }, {
        path: '/',
        name: 'frontpage',
        component: FrontPage,
    }, {
        path: '/profiles',
        name: '',
        component: Profiles,
        meta: {
            auth: true
        },
        children: [
            {
                path: '',
                name: 'profiles-list',
                component: ProfilesList,
                meta: {
                    auth: {
                        roles: ['admin', 'superadmin'],
                    },
                },
            }, {

                path: 'edit-template/:id',
                name: 'acc-template-create',
                component: AccTemplateCreate,
                props: true,
                meta: {
                    auth: {
                        roles: ['company'],
                    },
                },
            }, {
                path: 'manage/:acc',
                name: '',
                component: ProfileShow,
                props: true,
                meta: {
                    auth: {
                        roles: ['admin', 'superadmin', 'company', 'user'],
                    },
                },
                children: [
                    {
                        path: 'info',
                        name: 'profile-show',
                        component: AccountInfo,
                    }, {
                        path: 'templates',
                        name: 'account-templates',
                        component: AccountTemplates,
                        props: true,
                        meta: {
                            auth: {
                                roles: ['company'],
                            },
                        },
                    }, {
                        path: 'users',
                        name: 'account-users',
                        component: AccountUsers,
                        props: true,
                        meta: {
                            auth: {
                                roles: ['company'],
                            },
                        },
                    }
                ],
            }, {
                path: 'setup/:template',
                name: 'template-setup',
                component: TemplateSetup,
                props: true,
                meta: {
                    auth: {
                        roles: ['superadmin', 'company'],
                    },
                },
            }
        ],
    }, {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    }, {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    }, {
        path: '/templates',
        name:'',
        component: TemplateIndex,
        meta: {
            auth: {
                roles: ['admin', 'company'],
            },
        },
        children: [
            {
                path: 'group/:id',
                name: 'templates',
                component: TemplateList,
                props: true
            }, {
                path: 'edit/:id',
                name: 'template-edit',
                component: TemplateEdit,
                props: true,
                meta: { reuse: false },
            }, {
                path: 'create',
                name: 'template-create',
                props: true,
                component: TemplateCreate
            },
            // {
            //     path: 'units/:id',
            //     name: 'units-index',
            //     component: UnitsList,
            //     props: true,
            // }, {
            //     path: 'unit/edit/:id',
            //     name: 'units-edit',
            //     component: UnitsEdit,
            //     props: true
            // },
            // {
            //     path: 'unit/create',
            //     name: 'units-create',
            //     component: UnitsCreate,
            //     props: true
            // },
            {
                path: 'groups',
                name: 'groups-index',
                component: GroupsList,
            },{
                path: 'groups/:id',
                name: 'group-show',
                component: GroupTemplates,
                props: true
            },

        ]
    },  {
        path:"/catalogs",
        component: Catalogs,
        meta: {
            auth: {
                roles: ['superadmin', 'admin', 'company'],
            },
        },
        children: [
            {
                path: '',
                name: 'catalogs',
                component: CatalogsList
            },
            {
                path: 'show/:id',
                name: 'catalog-show',
                component: CatalogShow,
                props: (route) => ({
                    catalogName: route.params.catalogName
                })
            },
            {
                path: 'add',
                name: 'catalogs-add',
                component: CatalogUpload
            },

        ],
    }, {
        path:"/documents",
        component: DocumentIndex,
        meta: {
            auth: {
                roles: ['superadmin', 'company', 'user'],
            },
        },
        children: [
            {
                path: '',
                name: 'main-documents',
                component: TemplatesList
            },
            {
                path: 'custom',
                name: 'user-documents',
                component: UserTemplatesList
            },
            {
                path: 'list',
                name: 'documents-list',
                component: DocumentsList
            },
            {
                path: 'create/:tpl',
                name: 'document-create',
                component: DocumentCreate,
                props: true
            },
            {
                path: 'edit/:doc',
                name: 'document-edit',
                component: DocumentEdit,
                props: true
            },
            {
                path: 'show/:doc',
                name: 'document-show',
                component: DocumentShow,
                props: true,
            },
            {
                path: 'archive',
                name: 'documents-archive',
                component: DocumentsArchive,
            },
        ],
    },
];

// AppRoutes.afterEach(() => {
//   // On small screens collapse sidenav
//   if (window.layoutHelpers && window.layoutHelpers.isSmallScreen() && !window.layoutHelpers.isCollapsed()) {
//     setTimeout(() => window.layoutHelpers.setCollapsed(true, true), 10)
//   }
//
//   // Scroll to top of the page
//   globals().scrollTop(0, 0)
// })
//
// AppRoutes.beforeEach((to, from, next) => {
//   // Set loading state
//   document.body.classList.add('app-loading')
//
//   // Add tiny timeout to finish page transition
//   setTimeout(() => next(), 10)
// })

export default AppRoutes
