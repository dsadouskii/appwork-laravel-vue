import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import AppRoutes from './router/index';

import { store } from './store/store';

import axios from 'axios';
import VueAxios from 'vue-axios';

import BootstrapVue from 'bootstrap-vue'

import Toastr from 'vue-toastr';
import Multiselect from 'vue-multiselect';

import globals from './globals'
import Popper from 'popper.js'


window.vueToastr = Toastr;
Vue.use(window.vueToastr,  {
    defaultTimeout: 3000,
    defaultProgressBar: false,
    defaultProgressBarValue: 0,
    defaultType: "success",
    defaultPosition: "toast-top-right",
} );

Vue.use(VueRouter);
const router = new VueRouter({
    base: '/',
    mode: 'history',
    routes: AppRoutes
});
Vue.router = router;
App.router = Vue.router;


Vue.use(VueAxios, axios);

Vue.use(BootstrapVue);
Vue.component('multiselect', Multiselect)

Vue.use(require('@websanova/vue-auth'), {
    // fetchData: { enabled: true},
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    rolesVar: 'roles',
});

Vue.directive("toggle-open", {
    bind: function(el, binding, vnode) {
        el.addEventListener(
            "click",
            () => {
                let open = vnode.context.class_open;
                open = !open;
                vnode.context.class_open = open;
                if (!open) {
                    el.classList.remove("open");
                } else {
                    el.classList.add("open");
                }
            },
            false
        );
    }
});

Vue.filter('highlight', ( haystack, needle ) =>
    haystack.replace(new RegExp(needle, 'gi'), str => `<b class='highlight'>${str}</b>`));

Vue.filter('lastDays', ( date ) => {
    let expire = new Date( date );
    let now =  new Date();
    if (expire - now > Math.pow(10,10)) {
        return '∞';
    }
    return (expire > now) ? (Math.ceil((expire - now) / (1000 * 60 * 60 * 24)) + ' дней') : '0';
});

// Required to enable animations on dropdowns/tooltips/popovers
Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false

Vue.config.productionTip = false


// Global RTL flag
Vue.mixin({
    data: globals
})

const vm = new Vue({
    store,
    render: h => h(App)
}).$mount('#app');

export { vm };
