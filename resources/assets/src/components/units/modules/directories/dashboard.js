import template from './dashboard.html';
import {mapGetters} from 'vuex';

/**
 * Created by peak on 2017/2/10.
 */
export default {
    template,
    data(){
        return {
            placeholder: '',
            source: 0,
            directories:[],
            directory: false,
            column: false,
        }
    },
    computed:{
        ...mapGetters({
            appDir : 'getAppDirectories',
            accDir : 'getAccountDirectories',
        }),
    },
    created(){
        this.directories[0] = this.appDir;
        this.directories[1] = this.accDir;
    },
    methods: {
        showDirectories(){
            if (this.source === 1) {
                this.directories = this.appDir;
            } else {
                this.directories = this.accDir;
            }
        },
        insertInput(){
            let dirIds = this.directories[this.source][this.directory].id +
                '|'
                + this.directories[this.source][this.directory].columns[this.column].id;
            let input = '<input class="input" ' +
                    'data-type="directory" placeholder="' + this.placeholder + '"  ' +
                    'data-dir="' + dirIds + '" ' +
                    'data-placeholder="' + this.placeholder + '" readonly="">';

            this.$parent.execCommand('insertHTML', input);
            this.placeholder = '';
            this.source = 0;
            this.directory = this.column = false;
        },
    }
}