import dashboard from './dashboard'

export default {
    name: 'directories',
    icon: 'fa fa-university',
    i18n: 'directories',
    dashboard
}

