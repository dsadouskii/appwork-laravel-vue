import template from './dashboard.html'

/**
 * Created by peak on 2017/2/10.
 */
export default {
    template,
    data(){
        return {
            placeholder: 'введите текст',
            type: 'text',
            options:[
                '',
                '',
            ],
        }
    },
    methods: {
        insertInput(){
            let input = '';
            if (this.type === 'select'){
                input = '<select readonly class="input" data-type="select"'  +
                    '" data-placeholder="' + this.placeholder + '" required>';
                input += '<option value="" disabled selected hidden>' + this.placeholder + '</option>';
                for(let i = 0; i < this.options.length; i++){
                    input += '<option value="'+ this.options[i] +'">' + this.options[i] + '</option>'
                }
                input +='</select>';
            } else {
                input = '<input class="input" ' +
                    'data-type="'+ this.type +'" placeholder="' + this.placeholder + '"  ' +
                    'data-placeholder="' + this.placeholder + '" readonly="">';
            }

            this.$parent.execCommand('insertHTML', input);
            this.placeholder = 'введите текст';
            this.type = 'text';
            this.options = ['', '',];
        },
        setDefaultPlaceholder(){
            switch (this.type) {
                case 'text'    : this.placeholder = 'введите текст';break;
                case 'date'    : this.placeholder = 'укажите дату';break;
                case 'number'  : this.placeholder = 'введите число';break;
                case 'select'  : this.placeholder = 'выберите пункт';break;
            }
        }
    }
}