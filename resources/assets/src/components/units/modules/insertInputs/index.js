import dashboard from './dashboard'

export default {
    name: 'inputs',
    icon: 'fa fa-plus-square',
    i18n: 'inputs',
    dashboard
}

