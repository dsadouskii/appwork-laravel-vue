import dashboard from './dashboard'

export default {
    name: 'customtable',
    icon: 'fa fa-table',
    i18n: 'customtable',
    dashboard
}

