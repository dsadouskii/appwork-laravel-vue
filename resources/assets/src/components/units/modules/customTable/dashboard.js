import template from './dashboard.html'

/**
 * Created by peak on 2017/2/10.
 */
export default {
    template,
    data(){
        return {
            rows: 2,
            cols: 2,
            width:'',
            bordered: true,
            hasHead: false,
            striped: false,
            hover: false
        }
    },
    methods: {
        insertTable(){
            let widthArr = [];
            if(this.width){
                widthArr = this.width.split('/');
            }
            if (this.rows < 1 || this.rows > 20) {
                return
            }
            if (this.cols < 2 || this.cols > 10) {
                return
            }
            let table = '<table '+ (this.bordered ? 'class="bordered"' : '') +' style="border-collapse: collapse; width: 100%; max-width: 100%; margin-bottom: 0; background-color: transparent;"><tbody>'
            for (let i = 0; i < this.rows; i++) {
                table += '<tr>'
                for (let j = 0; j < this.cols; j++) {
                    table += '<td style="vertical-align: top;width: '+ ((widthArr[j] === 'undefined') ? 'auto;' : (widthArr[j]+'%;') ) +'"></td>'
                }
                table += '</tr>'
            }
            table += '</tbody></table>';
            this.$parent.execCommand('insertHTML', table)
        }
    }
}