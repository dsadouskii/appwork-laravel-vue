'use strict'
export default function initEvents() {
    /* -----datepicker-handler-----*/
    $('label.new.input[data-type="date"]').append('<div contenteditable="false" class="datepicker-container" style="display: none;"><div class="select-delivery-date-input"></div></div>');

    $('.select-delivery-date-input').datepicker({
        dateFormat:'dd.mm.yy',
        changeMonth: true,
        changeYear: true,
        showOn: 'none',
        onSelect: function(selectedDate){
            if ($(this).closest('label.input').contents()
                    .filter(function() {return this.nodeType === 3;})[0] === undefined)
            {
                var textNode = document.createTextNode(selectedDate);
                $(this).closest('label.input').append(textNode);
            } else {
                $(this).closest('label.input').contents()
                    .filter(function() {
                        return this.nodeType === 3; //Node.TEXT_NODE
                    })[0].nodeValue = selectedDate;
            }
            $(this).closest('.datepicker-container').hide();
        }
    });

    let dates = document.querySelectorAll('label.input[data-type="date"]');
    dates.forEach(label => {
        label.addEventListener('focusout', function (event) {
            let relElem = event.relatedTarget ? event.relatedTarget.closest('label.input') : event.relatedTarget;
            if (event.path.indexOf( this ) === 0 && relElem !== this) {
                this.querySelector('.datepicker-container').style.display = 'none';
            }
        });
        label.addEventListener('focusin', function (event) {
            this.querySelector('.datepicker-container').style.display = 'block';
        });
    });

    /* -----text-canvas-handler-----*/
    let texts = document.querySelectorAll('label.input[data-type="text"]');
    texts.forEach(label => {
        label.addEventListener('keydown', function (event) {
            if(this.innerText === this.getAttribute('data-placeholder')){
                this.innerText = '';
            }
        });
        label.addEventListener('keyup', function (event) {
            if(this.innerText === ''){
                this.innerText = this.getAttribute('data-placeholder');
            }
        });
        label.addEventListener('focusout', function (event) {
            if(this.innerText === ''){
                this.innerText = this.getAttribute('data-placeholder');
            }
        });
    });

    /*-----search-handler-----*/
    $('label.new.input[data-type="directory"]')
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                let element = $(this)[0].element;
                let directory = element.attr('data-dir').replace('|','/');
                axios.get('searchDir/' + directory + '?query=' + request.term)
                    .then(res => {
                        response(res.data)
                    }).catch(error => {
                    response(['Ашыпка! :-{']);
                });
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                $(this).html(ui.item.value);
            }
        });
    let selects = document.querySelectorAll('label.input[data-type="directory"]');
    selects.forEach(label => {
        label.addEventListener('keydown', function (event) {
            if(this.innerText === this.getAttribute('data-placeholder')){
                this.innerText = '';
            }
        });
        label.addEventListener('keyup', function (event) {
            if(this.innerText === ''){
                this.innerText = this.getAttribute('data-placeholder');
            }
        });
        label.addEventListener('focusout', function (event) {
            if(this.innerText === ''){
                this.innerText = this.getAttribute('data-placeholder');
            }
        });
    });

    /*-----numeric-handler-----*/
    let numbers = document.querySelectorAll('label.input[data-type="number"]');
    numbers.forEach(label => {
        label.addEventListener('keydown', function (event) {
            if(this.innerText === this.getAttribute('data-placeholder')){
                this.innerText = '';
            }
        });
        label.addEventListener('keyup', function (event) {
            if(this.innerText === ''){
                this.innerText = this.getAttribute('data-placeholder');
            }
        });
        label.addEventListener('focusout', function (event) {
            if(this.innerText === ''){
                this.innerText = this.getAttribute('data-placeholder');
            }
        });
    });

    $('#fill-document').on('keypress','label.input[data-type="number"]', function (event) {
        if ((event.which != 46 || $(this).text().indexOf('.') != -1)
            && (event.which < 48 || event.which > 57) ||
            (event.which == 46 && $(this).caret().start == 0) ) {
            event.preventDefault();
        }
    });

    $('#fill-document').on('keyup','label.input[data-type="number"]', function (event) {
        if($(this).text().indexOf('.') == 0) {
            $(this).text($(this).text().substring(1));
        }
    });

    $('#fill-document').on('focusout','label.input[data-type="number"]', function (event) {
        $(this).css({ 'margin-right' : '0'});
        let str = $(this).text();
        if (str === ''){
            $(this).text($(this).attr('data-placeholder'));
        } else if (str.charAt(str.length - 1) === '.') {
            $(this).text($(this).text().slice(0,-1));
        }
    });
    /*-------------------------------*/
    // $( 'label.input' ).tooltip();
    $('.new').removeClass('new');
}