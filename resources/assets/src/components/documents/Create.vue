<template>
    <b-container>
        <b-row class="justify-content-md-center">
            <b-col cols="12" md="auto">
                <h1>Создание документа</h1>
                <h5>Шаблон: {{template.name}}</h5>
            </b-col>
        </b-row>
        <b-row class="justify-content-md-center">
            <b-col cols="8">
                <b-input-group prepend="Документ:">
                    <b-form-input v-model="newDocument.name"
                                  type="text"
                                  style="z-index: 2;"
                                  placeholder="Служебное наименование"></b-form-input>
                    <b-input-group-append>
                        <b-btn variant="success"
                               @click="showDocument">
                            Сохранить
                        </b-btn>
                    </b-input-group-append>
                </b-input-group>
            </b-col>
        </b-row>
        <b-row class="mt-3">
            <b-col cols="4" class="left-menu" v-if="!loading">
                <div class="unit-menu">
                    <ul v-if="orders.length > 0">
                        <li v-for="(menu, pIndex) in orders" class="u-title" v-if="menu.units.length > 1">
                            <label>
                                {{menu.order_title}}
                            </label>
                            <ul v-if="menu.units">
                                <li v-for="(item, uIndex) in menu.units" class="u-item">
                                    <label>
                                        <input @click="prepareUnit(item, true)" :checked="activeUnits.findIndex(x => x.id === item.id) > -1"
                                               type="radio"
                                               :name="'order' + pIndex">
                                        {{item.name}}
                                    </label>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </b-col>
            <b-col cols="8">
                <div class="a-format">
                    <form id="fill-document" style="width: 16.5cm">
                        <div :class="{ 'unit' : true }"
                             v-for="(unit, uIndex) in activeUnits" :key="unit.id">
                            <div v-html="unit.content"></div>
                        </div>
                    </form>
                </div>
            </b-col>
        </b-row>
    </b-container>
</template>

<script>
    import initEvents from './fillHelper';
    export default {
        name: "doc-create",
        props: ['tpl'],
        data() {
            return {
                template:{
                    id: null,
                    title: '',
                },
                newDocument: {
                    content: '',
                    name: '',
                    user_id: null,
                    account: null,
                },
                orders:[],
                loading: true,
                activeUnits: [],
            }
        },
        created(){
            this.id = this.$route.params.tpl;
            this.newDocument.user_id = this.$auth.user().id;
            this.renderTemplate();
        },
        updated() {
            initEvents();
        },
        methods:{
            params: function () {
               console.log(this.activeUnits);
            },
            renderTemplate() {
                this.error = null;
                this.loading = true;
                axios.get('templates/' + this.id)
                    .then(response => {
                        this.template = response.data.data;
                        this.template.units.forEach(i => this.prepareUnit(i));
                        this.renderMenu();
                        this.loading = false;
                    }).catch(error => {
                    this.loading = false;
                    console.log(error);
                    this.error = error.response.message || error.message;
                });
            },
            renderMenu() {
                let unitList = this.template.units;
                for (let index in unitList){
                    if(unitList[index].order === this.orders.length) {
                        this.orders.push({'units': [],'order_title': unitList[index].order_title });
                        this.activeUnits.push(unitList[index]);
                    }
                    this.orders[unitList[index].order].units.push(unitList[index]);
                };
                initEvents();
                // this.inputList = JSON.parse(this.editableItem.inputs);
            },
            // cloneUnit: function (pIndex) {
            //     let searchId = this.activeParagraphs[pIndex]['units'][0].id;
            //     let p = this.paragraphs[pIndex];
            //     let newUnit = p['units'][p.units.findIndex(x => x.id === searchId)];
            //     this.activeParagraphs[pIndex]['units'].push(newUnit);
            // },
            deleteUnit: function (pIndex) {
                this.activeParagraphs[pIndex]['units'].splice(-1, 1);
            },
            prepareUnit: function (unit, replace = false) {
                const types = {
                    'text'   : 'Текстовое поле',
                    'number' : 'Введите число',
                    'select' : 'Выберите из списка',
                    'date'   : 'Выберите дату',
                    'directory' : 'Автозаполнение из справочника',
                };
                let renderBlock = document.createElement('div');
                let content = unit.content;
                renderBlock.innerHTML = content;
                let inputs = renderBlock.querySelectorAll('input.input');
                inputs.forEach(input => {
                    let forReplace = input.outerHTML;
                    let replace = document.createElement('label');
                    let type = input.getAttribute('data-type');
                    replace.setAttribute('data-type', type);
                    if (input.hasAttribute('data-dir')) {
                        replace.setAttribute('data-dir', input.getAttribute('data-dir'));
                    }
                    replace.setAttribute('contenteditable', 'true');
                    replace.setAttribute('title', types[type]);
                    replace.setAttribute('class', 'input new');
                    replace.setAttribute('data-placeholder', input.getAttribute('placeholder'));
                    replace.innerText = input.getAttribute('placeholder');
                    content = content.replace(forReplace, replace.outerHTML);
                });
                unit.content = content;
                if(replace){
                    this.activeUnits.splice(unit.order, 1);
                    this.activeUnits.splice(unit.order, 0, unit);
                }
            },
            showDocument: function () {
                let inputs = document.querySelectorAll('.input');
                let inputData = [];
                inputs.forEach(i => {
                    let value = '';
                    if(i.tagName === 'SELECT'){
                        value = i.value;
                    } else if (i.childNodes[0].nodeValue !== i.getAttribute('data-placeholder')){
                        value = i.childNodes[0].nodeValue;
                    }
                    inputData.push(value);
                });
                let formData = {};
                formData['name'] = this.newDocument.name;
                formData['units'] = this.activeUnits.map(function(unit) {
                    return unit.id;
                });
                formData['template_id'] = this.id;
                formData['replies'] = inputData;
                axios
                    .post('document', formData,)
                    .then(response => {
                        console.log(response.data);
                        this.$router.push({ name: 'document-show', params:{ doc: (response.data.data.id)} });
                    }).catch(error => {
                    this.errors = error.response.data.errors || [error.message];
                });
            },
        }
    }
</script>

<style scoped>

</style>