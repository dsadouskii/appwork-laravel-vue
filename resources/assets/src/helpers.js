export function documentMap(
    parentSelector = '.edit-field',
    keySelector = '.unit-head',
    elementsSelector = '.input',
    elementKey = 'data-key',
    elementValue = 'data-placeholder',
    ) {
    let fieldsObj = {}
    let fields = document.querySelectorAll(parentSelector);
    fields.forEach(field => {
        let fieldKey = field.querySelector(keySelector).value;
        console.log(fieldKey);
        let inputs = field.querySelectorAll(elementsSelector);
        let inputObj = {};
        inputs.forEach(input => {
            inputObj[input.getAttribute(elementKey)] = input.getAttribute(elementValue);
        });
        fieldsObj[fieldKey] = inputObj;
    });
    return fieldsObj;
}

export function parseChanges(oldVal, newVal) {
    let changes = {add:{},del:{}};
    const oldMap = JSON.parse(oldVal);
    const oldUnits = Object.keys(oldMap);
    const newMap = JSON.parse(newVal);
    const newUnits = Object.keys(newMap);
    const jointUnits = oldUnits.filter(x => newUnits.includes(x));
    const removedUnits = oldUnits.filter(x => !jointUnits.includes(x));
    const addedUnits = newUnits.filter(x => !jointUnits.includes(x));
    for (let i = 0; i < jointUnits.length; i++){
        const oldUnit = oldMap[jointUnits[i]];
        const oldUnitInputTags = Object.keys(oldUnit);
        const newUnit = newMap[jointUnits[i]];
        const newUnitInputTags = Object.keys(newUnit);
        const removedInputs = oldUnitInputTags.filter(x => !newUnitInputTags.includes(x));
        const addedInputs = newUnitInputTags.filter(x => !oldUnitInputTags.includes(x));
        changes.del[jointUnits[i]] = removedInputs;
        changes.add[jointUnits[i]] = addedInputs;
    }

    let log = '';
    if (removedUnits.length > 0) {
        log += '<p>Удалены разделы: <b>' + removedUnits.join(', ') + '</b>.</p>';
    }
    if (addedUnits.length > 0) {
        log += '<p>Добавлены разделы: <b>' + addedUnits.join(', ') + '</b>.</p>';
    }
    for( let key in changes.add) {
        if(changes.add[key].length > 0){
            log += '<p>В разделе "' + key + '" добавлены поля: <b style="color: #67b168">';
            let logKeys = changes.add[key];
            for (let i = 0; i < logKeys.length; i++){
                log += newMap[key][logKeys[i]];
                if((i + 1) === logKeys.length){
                    log += '.';
                } else {
                    log += ', ';
                }
            }
            log += '</b></p>';
        }
    }
    for( let key in changes.del) {
        if(changes.del[key].length > 0){
            log += '<p>В разделе "' + key + '" удалены поля: <b style="color: #b16867">';
            let logKeys = changes.del[key];
            for (let i = 0; i < logKeys.length; i++){
                log += oldMap[key][logKeys[i]];
                if((i + 1) === logKeys.length){
                    log += '.';
                } else {
                    log += ', ';
                }
            }
            log += '</b></p>';
        }
    }
    console.log(log);
    console.log(removedUnits);
    console.log(addedUnits);
    console.log(changes);
    return log;
}